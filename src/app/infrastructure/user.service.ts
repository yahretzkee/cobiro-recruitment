import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { User } from "./user";

@Injectable(
    { 'providedIn': 'root' }
)
export class UserService {

    constructor(private httpClient: HttpClient) {

    }

    userSignIn(user: User) {
        return this.httpClient.post(environment.base_url + 'login', {
            data: {
                type: 'login',
                attributes: {
                    email: user.email,
                    password: user.password
                }
            }
        });
    }

}