import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../infrastructure/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  userForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  private subscription = new Subscription();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  signIn() {
    this.subscription.add(
      this.userService.userSignIn(
        {
          email: this.userForm.get('email').value,
          password: this.userForm.get('password').value
        }
      ).subscribe());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
